const { renderGraphFromSource } = require("graphviz-cli");
const fs = require("fs");

// récupération param dans la command pour spécifier le chemin vers le fichier de graph
const filePath = process.argv[2];

// check que le fichier existe bien, sinon errur
if (!fs.existsSync(filePath)) {
	console.log("\x1b[31m%s\x1b[0m", `File not found: ${filePath}`);
	process.exit(0);
}

// on coupe le fichier par ligne pour le parser ensuite
const file = fs.readFileSync(filePath, { encoding: "utf8", flag: "r" });
const lines = file.split("\n\t");

// contiendra les lignes légitime (qui concernent un cycle)
const newLines = [];
lines.forEach(line => {
	if (line.indexOf(`[root]`) === -1 || line.indexOf(`[color = "red"`) > -1) {
		newLines.push(line);
	}
});
(async () => {
	// transforme le graph parsé en image SVG
	await renderGraphFromSource(
		{ input: newLines.join("\r\n") },
		{ format: "svg", name: `${filePath}.svg` }
	);

	console.log("\x1b[32m%s\x1b[0m", `File ${filePath}.svg created successfully`);
})();
