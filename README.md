# Outil pour voir le graphique d'un cycle Terraform

* Générer un fichier de graph dans votre projet (ou via le ci/cd)

```bash
terraform plan -out=plan
terraform graph -draw-cycles -type=apply -plan=plan > plan.graph
```

* Récupérer le fichier généré _plan.graph_

* Le placer dans le repertoire files de ce projet

* Initialiser le projet avec un `npm install`

* Lancer la script `start` présent dans le package.json en changeant le fichier d'entrée

* Un fichier svg vient d'être crée
